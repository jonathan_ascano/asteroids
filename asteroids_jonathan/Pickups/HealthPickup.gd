extends Area2D

var isDestroyed := false

func destroy():
	if isDestroyed:
		return
	
	isDestroyed = true
	
	get_parent().remove_child(self)
	queue_free()

func _on_HealthPickup_body_entered(body):
	if body.is_in_group("Player"):
		body.gainHealth(1)
		destroy()

func _on_Timer_timeout():
	print("HP pickup timed out!")
	destroy()
